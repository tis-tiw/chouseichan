package jp.co.tis.tiw.chouseichan.dao;

import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.boot.ConfigAutowireable;

import jp.co.tis.tiw.chouseichan.entity.Vote;

/**
 * 参加可否DAO
 */
@ConfigAutowireable
@Dao
public interface VoteDao {

    /**
     * 参加可否を挿入する。
     * @param vote 参加可否
     * @return 挿入件数
     */
    @Insert
    int insert(Vote vote);
}
