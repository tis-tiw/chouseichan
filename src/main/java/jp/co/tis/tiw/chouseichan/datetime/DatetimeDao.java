package jp.co.tis.tiw.chouseichan.datetime;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;
import org.seasar.doma.boot.ConfigAutowireable;

import java.util.Map;

/**
 * DBからシステム日時を取得するDAOクラス。
 * 環境構築後の動作確認に使用する。
 * （アプリケーションとしては使用しない）
 */
@ConfigAutowireable
@Dao
public interface DatetimeDao {

    /**
     * 日時を取得する。
     * @return 日時
     */
    @Select
    Map<String, Object> select();
}
